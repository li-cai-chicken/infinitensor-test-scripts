## 使用说明

### get_model_profiling.py
#### Runbook
这个脚本用于获取模型训练过程中各个环节的性能数据， 运行`python get_model_profiling.py` 可以得到一些性能分析的结果。
目前这个脚本只支持在GPU和寒武纪上运行，可以通过修改`paddle.device.set_device("gpu")`完成平台的切换。
在`profiler.Profiler(scheduler = [3,14], on_trace_ready=my_on_trace_ready, timer_only=True)` 中，我们可以通过控制time_only这个参数来控制输出的性能测试报告细节：
如果是`True`的话，只输出最后的读取数据和训练的时间；如果是`False`的话，会输出每个环节的性能数据，会有CPU/加速卡占用的各种分析指标，以及在不同算子中消耗的时间。
#### Issue
如果遇到报错Could not load dynamic library libcupti.so.10.1， 参考[Paddle profiling 4.3](https://www.paddlepaddle.org.cn/documentation/docs/zh/guides/performance_improving/profiling_model.html#kenengchuxiandehuanjingwenti)解决。

### test_paddle.py
#### Runbook
这个脚本用于测试PaddlePaddle能否在各种平台上运行，脚本的主要内容是训练一个LeNet模型，将模型在测试集上验证并保存模型。
运行命令：`python test_paddle.py --device=[CUDA|KUNLUN|BANG|NPU|CPU]` 其中device的参数对应的是运行的设备平台，
例如`--device=CUDA` 表示在GPU上运行，`KUNLUN` 代表昆仑芯，`BANG` 代表寒武纪，`--device=NPU` 代表昇腾。

### test_image_classification.py
#### Runbook
这个脚本用于测试InfiniTensor与PaddlePaddle在GPU平台上进行推理性能比较，脚本的主要内容从ImageNet数据集中读取图片，进行预处理，然后通过AI框架（PaddlePaddle、InfiniTnesor）加载模型，
对模型输入图片拿到输出，最后统计推理时间。
运行命令 `python test_image_classification.py` 可以默认参数执行脚本。一般情况下，需要指定model_file, data_path_suffix和label_path_suffix三个参数。
以下是各参数的介绍
```
python test_image_classification.py --help
usage: test_image_classification.py [-h] [--base_model_path [BASE_MODEL_PATH]] [--model_file [MODEL_FILE]] [--base_path [BASE_PATH]] [--data_path_suffix [DATA_PATH_SUFFIX]]
                                    [--label_path_suffix [LABEL_PATH_SUFFIX]] [--output_folder_name_prefix [OUTPUT_FOLDER_NAME_PREFIX]] [--test_tensor_sense {False,True}]

options:
  -h, --help            show this help message and exit
  --base_model_path [BASE_MODEL_PATH]
                        directory path which we store our models
  --model_file [MODEL_FILE]
                        Use which model to predict and test performance
  --base_path [BASE_PATH]
                        Base path which we run our project
  --data_path_suffix [DATA_PATH_SUFFIX]
                        directory path which we store our data
  --label_path_suffix [LABEL_PATH_SUFFIX]
                        file path which we store our class file
  --output_folder_name_prefix [OUTPUT_FOLDER_NAME_PREFIX]
                        directory path which we store our output file
  --test_tensor_sense {False,True}

```
### test_image_classification_correctness.py
#### Runbook
这个脚本用于测试InfiniTensor推理结果的正确性，它将读取随机生成的数据，然后加载模型，分别使用InfiniTensor、PaddlePaddle和OnnxRuntime进行推理，并将结果进行比较。
如果两个框架输出的结果差距很小，会打印测试成功语句`The difference of results between ... and ... looks good!`,否则会打印报错日志和两个框架输出的结果。
运行命令 `python test_image_classification_correctness.py`会执行测试脚本，需要在当前路径下有models文件夹，文件夹里面有onnx和paddle模型文件。