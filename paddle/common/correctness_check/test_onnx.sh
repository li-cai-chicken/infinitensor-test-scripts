
device=GPU
path=/models/TorchVision/ 
for file in ${path}*.onnx;
do
	python onnx_inference.py $file $device
done

