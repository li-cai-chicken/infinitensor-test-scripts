import sys
import time
import onnx
import numpy as np
from pyinfinitensor.onnx import OnnxStub, backend
import onnxruntime
import os


def test_onnx_inf(model_path):
    print('******************************Start to infer with a new model*****************************************')
    print(model_path)
    # We could skip some models using this logic 
    # if 'vit_' not in path:
    #     return
    onnx_model = onnx.load(model_path)
    onnx_input = onnx_model.graph.input[0]
    input_shape = [[d.dim_value for d in _input.type.tensor_type.shape.dim]
                   for _input in onnx_model.graph.input]

    # Assume that there is only one input tensor
    input_shape = input_shape[0]
    print("input_shape:{}".format(input_shape))
    input_data = np.random.random(input_shape).astype(np.float32)

    if device == "GPU":
        model = OnnxStub(onnx_model, backend.cuda_runtime())
    elif device == "MLU":
        model = OnnxStub(onnx_model, backend.bang_runtime())
    elif device == "XPU":
        model = OnnxStub(onnx_model, backend.kunlun_runtime())
    else:
        model = OnnxStub(onnx_model, backend.cpu_runtime())
    next(iter(model.inputs.values())).copyin_numpy(input_data)

    model.tune()
    model.run()
 
    outputs = next(iter(model.outputs.values())).copyout_numpy()
    outputs = np.array(outputs)
    print("outputs_shape:{}".format(outputs.shape))

    ort_sess = onnxruntime.InferenceSession(model_path)
    ort_inputs = {ort_sess.get_inputs()[0].name: input_data}
    ort_outs = ort_sess.run(None, ort_inputs)

    # compare ONNXRuntime and InfiniTensor results
    try:
        np.testing.assert_allclose(ort_outs[0].reshape([1000]), outputs.reshape([1000]), rtol=1.0, atol=1e-05)
        print("The difference of results between ONNXRuntime and InfiniTensor looks good!")   
    except AssertionError as e:
        print("Onnx and Infinitensor compare gap:" + str(e))

    print('------------------ End -------------------------')


if __name__ == '__main__':
    args = sys.argv
    if len(sys.argv) != 3:
        print("Usage: python onnx_inference.py model_name.onnx gpu")
        exit()
    path = sys.argv[1]
    device = sys.argv[2]

    if device == "GPU":
        os.environ["CUDA_VISIBLE_DEVICES"] = '1'
    elif device == "MLU":
        os.environ["MLU_VISIBLE_DEVICES"] = '1'
    elif device == "XPU":
        os.environ["XPU_VISIBLE_DEVICES"] = '1'
    else:
        pass
    
    if os.path.isdir(path):
        for file in os.listdir(path):
            file_path = os.path.join(path, file)
            _, ext = os.path.splitext(file_path)

            if ext == '.onnx':
                
                test_onnx_inf(file_path)
            time.sleep(0.5)
    else:
        print(path)
        test_onnx_inf(path)
