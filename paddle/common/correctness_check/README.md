### How to use these code to check your model correctness?
1. Edit `test_onnx.sh` file and change model fofler path, device line according to your case. For device, you could choose between CPU, GPU, MLU and XPU.
   ```
   device=GPU
   path=/models/TorchVision/ 
   ```
2. Run `sh ./test_onnx.sh` then this script will test your models one by one; you can also run this command `nohup sh ./test_onnx.sh > output.log 2>&1 &` to test model as a deamon process.