import argparse
import paddle
import numpy as np
from paddle.vision.transforms import Normalize

def parse_arg():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("--device",
                        type=str,
                        default="CPU",
                        choices=["CPU", "CUDA", "KUNLUN", "BANG", "NPU"])

    return parser.parse_args()

def run_mnist_train_and_infer(device):

    if device == "CUDA":
        paddle.device.set_device("gpu")
    elif device == "BANG":
        paddle.device.set_device("mlu")
    elif device == "KUNLUN":
        paddle.device.set_device("xpu")
    elif device == "NPU":
        paddle.device.set_device("npu")
    else:
        paddle.device.set_device("cpu")

    transform = Normalize(mean=[127.5], std=[127.5], data_format='CHW')
    # 下载数据集并初始化 DataSet
    train_dataset = paddle.vision.datasets.MNIST(mode='train', transform=transform)
    test_dataset = paddle.vision.datasets.MNIST(mode='test', transform=transform)

    # 模型组网并初始化网络
    lenet = paddle.vision.models.LeNet(num_classes=10)
    model = paddle.Model(lenet)

    # 模型训练的配置准备，准备损失函数，优化器和评价指标
    model.prepare(paddle.optimizer.Adam(parameters=model.parameters()), 
                paddle.nn.CrossEntropyLoss(),
                paddle.metric.Accuracy())

    # 模型训练
    model.fit(train_dataset, epochs=5, batch_size=64, verbose=1)
    # 模型评估
    model.evaluate(test_dataset, batch_size=64, verbose=1)

    # 保存模型
    model.save('./output/mnist')
    # 加载模型
    model.load('output/mnist')

    # 从测试集中取出一张图片
    img, label = test_dataset[0]
    # 将图片shape从1*28*28变为1*1*28*28，增加一个batch维度，以匹配模型输入格式要求
    img_batch = np.expand_dims(img.astype('float32'), axis=0)

    # 执行推理并打印结果，此处predict_batch返回的是一个list，取出其中数据获得预测结果
    out = model.predict_batch(img_batch)[0]
    pred_label = out.argmax()
    print('true label: {}, pred label: {}'.format(label[0], pred_label))
    # 可视化图片
    # from matplotlib import pyplot as plt
    # plt.imshow(img[0])

if __name__ == "__main__":
    arg_dict = parse_arg()
    run_mnist_train_and_infer(arg_dict.device)