import onnx
from pyinfinitensor.onnx import OnnxStub, backend
import numpy as np
import paddle
import time
import onnxruntime
# from onnx import ModelProto, NodeProto
# from onnxsim import simplify

models = [
    "onnxfile_complementary/alexnet_bs1.onnx",
    "onnxfile_complementary/googlenet_bs1.onnx",
    "onnxfile_complementary/squeezenet1_0_bs1.onnx",
    "onnxfile_complementary/vgg11_bs1.onnx",
    "onnxfile_complementary/resnext50_32x4d_bs1.onnx",
    "onnxfile_complementary/convnext_tiny_bs1.onnx",
    "onnxfile/resnet18-v2-7.onnx",
    "onnxfile/inception-v2-9.onnx",
    "onnxfile/efficientnet-lite4-11.onnx",
    "onnxfile/mobilenetv2-7.onnx",
    "onnxfile/densenet-12.onnx",
    "onnxfile/vgg16-12.onnx",
    "onnxfile/shufflenet-v2-10.onnx",
    "onnxfile/googlenet-3.onnx",
    "onnxfile/bvlcalexnet-12.onnx",
    "onnxfile/squeezenet1.1-7.onnx",
    "onnxfile/zfnet512-12.onnx",
    # "lenet.onnx",
    "onnxfile/caffenet-12.onnx"
]


for model_name in models:
    try:
        print("-------------------------------------------------------------------------------")
        print("Test {}".format(model_name))
        model = onnx.load(model_name)
        # TODO: Add supports for other devices
        # model, check = simplify(model)
        gofusion_model = OnnxStub(model, backend.cuda_runtime())
        model = gofusion_model
        model.init()
        x = np.random.random((1, 3, 224, 224)).astype('float32')
        x_tensor = paddle.to_tensor(x)
        next(model.inputs.items().__iter__())[1].copyin_float(x_tensor.reshape([3*224*224]).tolist())
        start_time = time.time()
        model.run()
        end_time = time.time()
        outputs = next(model.outputs.items().__iter__())[1].copyout_float()
        print('Exported {} Predict finished by Infinitensor!'.format(model_name))


        # predict by ONNXRuntime
        ort_sess = onnxruntime.InferenceSession(model_name)
        if model_name == "onnxfile/efficientnet-lite4-11.onnx":
            ort_inputs = {ort_sess.get_inputs()[0].name: x.reshape([1, 224, 224, 3])}
        else:
            ort_inputs = {ort_sess.get_inputs()[0].name: x}
        ort_outs = ort_sess.run(None, ort_inputs)
        x = x.reshape([1, 3, 224, 224])
        print(("Exported {} has been predicted by ONNXRuntime!").format(model_name))


        # compare ONNXRuntime and InfiniTensor results
        try:
            np.testing.assert_allclose(ort_outs[0].reshape([1000]), outputs, rtol=1.0, atol=1e-05)
            print("The difference of results between ONNXRuntime and InfiniTensor looks good!")
        except AssertionError as e:
            print("Onnx and Infinitensor compare gap:" + str(e))


    except Exception as e:
        print("AI frameworkwork has some issues during inference for model {}.".format(model_name))
        print(str(e))
