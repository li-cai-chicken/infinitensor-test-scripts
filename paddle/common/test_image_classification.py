import os

import argparse
import cv2
import numpy as np
from paddle.io import Dataset
from paddle.vision import transforms as T
import paddle
import time

SPLIT = "/"

transform = T.Compose([
    T.Resize((256, 256)),
    T.CenterCrop(224),
    T.Normalize(mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
                data_format="HWC")
])
batch_size = 1

class ImageNetDataset(Dataset):
    def __init__(self, data_dir, labels_path, transform=None):
        super().__init__()
        self.labels_dict = generate_classes(labels_path)
        self.data_list = []
        for file_name in os.listdir(data_dir):
            data_path = data_dir + "/" + file_name
            label = self.labels_dict[file_name.split("_")[2]]
            self.data_list.append([data_path, label])
        self.transform = transform

    def __getitem__(self, index):
        data_path, label = self.data_list[index]
        image = cv2.imread(data_path)
        image = image.astype('float32')
        image = image / 255.

        if self.transform is not None:
            image = transform(image)
        data = np.transpose(image, (2, 0, 1))
        return data, label

    def __len__(self):
        return len(self.data_list)


def generate_classes(class_file_name):
    with open(class_file_name, "r", encoding="utf-8") as f:
        lines = f.readlines()
        labels_dict = {}
        for index, line in enumerate(lines):
            name = line.strip().split()[1]
            label = line.strip().split("_")[2].split(".")[0]
            labels_dict[label] = int(name)
    return labels_dict


def export_to_file(model_name, content, arg_dict):
    output_folder_name = arg_dict.output_folder_name_prefix
    file_name = output_folder_name + SPLIT + model_name.split(SPLIT)[0] + ".md"
    with open(file_name, "w", encoding="utf-8") as f:
        f.write("#### 测试结果\n")
        f.write(content)


def parse_arg():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("--base_model_path",
                        type=str,
                        default="./models",
                        nargs="?",
                        help="directory path which we store our models")
    parser.add_argument("--model_file",
                        type=str,
                        default="./model_path.txt",
                        nargs="?",
                        help="Use which model to predict and test performance"
                        )
    parser.add_argument("--base_path",
                        type=str,
                        default="./",
                        nargs="?",
                        help="Base path which we run our project")
    parser.add_argument("--data_path_suffix",
                        type=str,
                        default="./datasets/ImageNet/Imagenet_val",
                        nargs="?",
                        help="directory path which we store our data")
    parser.add_argument("--label_path_suffix",
                        type=str,
                        default="./datasets/ImageNet/val_list.txt",
                        nargs="?",
                        help="file path which we store our class file")
    parser.add_argument("--output_folder_name_prefix",
                        type=str,
                        default="./output",
                        nargs="?",
                        help="directory path which we store our output file")
    parser.add_argument("--test_tensor_sense",
                        type=str,
                        default="True",
                        choices=["False", "True"])

    return parser.parse_args()


def post_process(result, top_k=5):
    # choose topk index and score
    scores = result.flatten()
    topk_indices = np.argsort(-1 * scores)[:top_k]
    topk_scores = scores[topk_indices]
    print("TopK Indices: ", topk_indices)
    print("TopK Scores: ", topk_scores)


def test_image_classification_performance(base_model_path, model_name, args_dict):
    model_dir = base_model_path + SPLIT + model_name
    data_path = args_dict.base_path + SPLIT + args_dict.data_path_suffix
    label_path = args_dict.base_path + SPLIT + args_dict.label_path_suffix
    dataset = ImageNetDataset(data_dir=data_path, labels_path=label_path, transform=transform)
    test_loader = paddle.io.DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=0, drop_last=True)
    model = paddle.jit.load(os.path.join(model_dir, "model"))
    total_time = 0
    total_size = 0
    total_acc = 0.0
    num = 0
    for batch_id, data in enumerate(test_loader()):
        images, labels = data
        images = paddle.to_tensor(images)
        if model_name == "efficient-lite4_model/efficient-lite4_model/inference_model":
           images = paddle.reshape(images, [1, 224, 224, 3])
        labels = paddle.to_tensor(labels)
        labels = paddle.reshape(labels, [-1, 1])
        start_time = time.time()
        result = model(images)
        end_time = time.time()
        total_time += end_time - start_time
        total_size += batch_size
        result = paddle.reshape(result, [1, 1000])
        acc = paddle.metric.accuracy(result, labels, k=5)
        total_acc += acc
        num += 1
        if num == 200:
            break

    print("batch_size: {}, average infer time: {}".format(batch_size, total_time / total_size))
    print("batch_size: {}, predictions top5 accuracy: {}".format(batch_size, total_acc.item() / num))

    content = "batch_size: {}, average infer time: {}".format(batch_size, total_time / total_size)
    export_to_file(model_name, content, args_dict)

def evaluateTensorSense(base_model_path, onnx_model_name, args_dict):
    from pyinfinitensor.onnx import OnnxStub, backend
    import onnx

    data_path = args_dict.base_path + SPLIT + args_dict.data_path_suffix
    label_path = args_dict.base_path + SPLIT + args_dict.label_path_suffix
    dataset = ImageNetDataset(data_dir=data_path, labels_path=label_path, transform=transform)
    test_loader = paddle.io.DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=0, drop_last=True)
    model_path = base_model_path + SPLIT + onnx_model_name
    model = onnx.load(model_path)
    # Need to support more devices
    gofusion_model = OnnxStub(model, backend.cuda_runtime())
    model = gofusion_model
    model.init()

    total_size = 0  # 总共的图片数
    total_time = 0.0
    total_acc = 0.0
    # 使用InfiniTensor的 Runtime 运行刚才加载并转换的模型, 验证是否一致
    for data in test_loader():
        images, labels = data
        next(model.inputs.items().__iter__())[1].copyin_float(images.reshape([3*224*224]).tolist())
        start_time = time.time()
        model.run()
        end_time = time.time()
        outputs = next(model.outputs.items().__iter__())[1].copyout_float()
        outputs = paddle.to_tensor(outputs)
        outputs = paddle.reshape(outputs, (1, 1000))
        total_time += (end_time - start_time)
        labels = paddle.reshape(labels, (1,1))
        acc = paddle.metric.accuracy(outputs, labels, k=5)
        total_acc += acc
        total_size += batch_size
        if total_size == 200:
            break


    print("batch_size: {}, average infer time: {}".format(batch_size, total_time / total_size))
    print("batch_size: {}, predictions top5 accuracy: {}".format(batch_size, total_acc.item() / total_size))


if __name__ == "__main__":
    arg_dict = parse_arg()
    file_name = arg_dict.model_file
    f = open(file_name, "r")
    model_names = []
    for line in f.readlines():
        model_name, onnx_model_name = line.strip().split(" ")
        model_names.append((model_name, onnx_model_name))

    for model_name, onnx_model_name in model_names:
            print("Will test model in this model path:" + arg_dict.base_model_path + "/" + model_name)
            test_image_classification_performance(arg_dict.base_model_path, model_name, arg_dict)
            if arg_dict.test_tensor_sense == "True":
                evaluateTensorSense(arg_dict.base_model_path, onnx_model_name, arg_dict)
            print("-----------------------------------------------------------------------------")


