import argparse
import onnx
from pyinfinitensor.onnx import OnnxStub, backend
import numpy as np
import onnxruntime
import onnx

def parse_arg():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("--device",
                        type=str,
                        default="CUDA",
                        nargs="?",
                        help="which device we are running on")
    parser.add_argument("--model_name",
                        type=str,
                        default="onnxfile/resnet18-v2-7.onnx",
                        nargs="?",
                        help="which model we are running on")
    return parser.parse_args()


# Save calculation data of each layer
def onnx_layer_output(onnx_path, dummy_input):
    providers = ["CPUExecutionProvider"]
    ort_session = onnxruntime.InferenceSession(onnx_path, providers=providers)
    org_outputs = [x.name for x in ort_session.get_outputs()]
    model = onnx.load(onnx_path)
    for node in model.graph.node:
        for output in node.output:
            if output not in org_outputs:
                model.graph.output.extend([onnx.ValueInfoProto(name=output)])
    # excute onnx
    ort_session = onnxruntime.InferenceSession(model.SerializeToString(), providers=providers)
    outputs = [x.name for x in ort_session.get_outputs()]
    ort_inputs = {ort_session.get_inputs()[0].name: dummy_input}
    ort_outs = ort_session.run(outputs, ort_inputs)
    ort_outs = zip(outputs, ort_outs)
    for i in ort_outs:
        print(i)

def test_model(model_name, device):
    print("-------------------------------------------------------------------------------")
    print("Test {}".format(model_name))
    onnx_model = onnx.load(model_name)
    # TODO: Add supports for other devices
    # model, check = simplify(model)
    if device == "CUDA":
        gofusion_model = OnnxStub(onnx_model, backend.cuda_runtime())
    elif device == "BANG":
        gofusion_model = OnnxStub(onnx_model, backend.bang_runtime())
    else:
        gofusion_model = OnnxStub(onnx_model, backend.cpu_runtime())

    model = gofusion_model
    model.init()
    x = np.random.random((1, 3, 224, 224)).astype('float32')
    # x_tensor = paddle.to_tensor(x)
    next(model.inputs.items().__iter__())[1].copyin_float(x.reshape([3*224*224]).tolist())
    model.run()
    outputs = next(model.outputs.items().__iter__())[1].copyout_float()
    print('Exported {} Predict finished by Infinitensor!'.format(model_name))

    # predict by ONNXRuntime
    ort_sess = onnxruntime.InferenceSession(model_name)
    if model_name == "onnxfile/efficientnet-lite4-11.onnx":
        ort_inputs = {ort_sess.get_inputs()[0].name: x.reshape([1, 224, 224, 3])}
    else:
        ort_inputs = {ort_sess.get_inputs()[0].name: x}
    ort_outs = ort_sess.run(None, ort_inputs)
    x = x.reshape([1, 3, 224, 224])
    print(("Exported {} has been predicted by ONNXRuntime!").format(model_name))

    # compare ONNXRuntime and InfiniTensor results
    try:
        np.testing.assert_allclose(outputs, ort_outs[0].reshape([1000]), rtol=3e-03, atol=0)
        print("The difference of results between ONNXRuntime and InfiniTensor looks good!")
    except AssertionError as e:
        print("Onnx and Infinitensor compare gap:" + str(e))

if __name__ == "__main__":
    arg_dict = parse_arg()
    test_model(arg_dict.model_name, arg_dict.device)
