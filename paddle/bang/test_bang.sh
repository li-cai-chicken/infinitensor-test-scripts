MLU_VISIBLE_DEVICES=1 python test_bang.py --device=BANG --model_name=onnxfile/resnet18-v2-7.onnx
MLU_VISIBLE_DEVICES=1 python test_bang.py --device=BANG --model_name=onnxfile/inception-v2-9.onnx
MLU_VISIBLE_DEVICES=1 python test_bang.py --device=BANG --model_name=onnxfile/efficientnet-lite4-11.onnx
MLU_VISIBLE_DEVICES=1 python test_bang.py --device=BANG --model_name=onnxfile/mobilenetv2-7.onnx
MLU_VISIBLE_DEVICES=1 python test_bang.py --device=BANG --model_name=onnxfile/densenet-12.onnx
