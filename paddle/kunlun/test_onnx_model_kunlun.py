import argparse
import onnx
from pyinfinitensor.onnx import OnnxStub, backend
import numpy as np
import onnxruntime

def parse_arg():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("--device",
                        type=str,
                        default="CUDA",
                        nargs="?",
                        help="which device we are running on")
    parser.add_argument("--model_name",
                        type=str,
                        default="onnxfile/resnet18-v2-7.onnx",
                        nargs="?",
                        help="which model we are running on")
    return parser.parse_args()
# arg_dict = parse_arg()

models = [
    "onnxfile/export_mobile_onnx.onnx",
    "onnxfile/export_lc_onnx.onnx"
]


def test_model(model_name, device):

    try:
        print("-------------------------------------------------------------------------------")
        print("Test {}".format(model_name))
        model = onnx.load(model_name)
        # TODO: Add supports for other devices
        # model, check = simplify(model)
        if device == "CUDA":
            gofusion_model = OnnxStub(model, backend.cuda_runtime())
        elif device == "BANG":
            gofusion_model = OnnxStub(model, backend.bang_runtime())
        elif device == "KUNLUN":
            gofusion_model = OnnxStub(model, backend.kunlun_runtime())
        else:
            gofusion_model = OnnxStub(model, backend.cpu_runtime())

        model = gofusion_model
        model.init()
        x = np.random.random((1, 3, 224, 224)).astype('float32')
        next(model.inputs.items().__iter__())[1].copyin_float(x.reshape([3*224*224]).tolist())
        model.run()
        outputs = next(model.outputs.items().__iter__())[1].copyout_float()
        print('Exported {} Predict finished by Infinitensor!'.format(model_name))


        # predict by ONNXRuntime
        ort_sess = onnxruntime.InferenceSession(model_name)
        if model_name == "onnxfile/efficientnet-lite4-11.onnx":
            ort_inputs = {ort_sess.get_inputs()[0].name: x.reshape([1, 224, 224, 3])}
        else:
            ort_inputs = {ort_sess.get_inputs()[0].name: x}
        ort_outs = ort_sess.run(None, ort_inputs)
        x = x.reshape([1, 3, 224, 224])
        print(("Exported {} has been predicted by ONNXRuntime!").format(model_name))


        # compare ONNXRuntime and InfiniTensor results
        try:
            np.testing.assert_allclose(ort_outs[0].reshape([102]), outputs, rtol=1.0, atol=1e-05)
            print("The difference of results between ONNXRuntime and InfiniTensor looks good!")
        except AssertionError as e:
            print("Onnx and Infinitensor compare gap:" + str(e))

    except Exception as e:
        print("AI frameworkwork has some issues during inference for model {}.".format(model_name))
        print(str(e))

if __name__ == "__main__":
    arg_dict = parse_arg()
    test_model(arg_dict.model_name, arg_dict.device)
