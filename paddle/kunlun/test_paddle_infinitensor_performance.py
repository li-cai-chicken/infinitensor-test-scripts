import os
import argparse
import numpy as np

SPLIT = "/"
batch_size = 1
def parse_arg():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("--base_model_path",
                        type=str,
                        default="./models",
                        nargs="?",
                        help="directory path which we store our models")
    parser.add_argument("--model_file",
                        type=str,
                        default="./model_path.txt",
                        nargs="?",
                        help="Use which model to predict and test performance"
                        )
    parser.add_argument("--base_path",
                        type=str,
                        default=".",
                        nargs="?",
                        help="Base path which we run our project")
    parser.add_argument("--data_file",
                        type=str,
                        default="imagenet_images.npy",
                        nargs="?",
                        help="directory path which we store our data")
    parser.add_argument("--label_file",
                        type=str,
                        default="imagenet_labels.npy",
                        nargs="?",
                        help="file path which we store our class file")
    parser.add_argument("--test_infini_tensor",
                        type=str,
                        default="True",
                        choices=["False", "True"])
    parser.add_argument("--test_paddle",
                        type=str,
                        default="False",
                        choices=["False", "True"])

    return parser.parse_args()

def post_process(output, top_k=5, labels=[0]):
    sorted_indices = np.argsort(output)[::-1]

    top_k_indices = sorted_indices[:top_k]
    top_k_accuracy =  1 if label[0] in top_k_indices else 0
    return top_k_accuracy

def evaluate_paddle(base_model_path, model_name, args_dict):
    import paddle

    model_dir = base_model_path + SPLIT + model_name
    data_path = args_dict.base_path + SPLIT + args_dict.data_file
    label_path = args_dict.base_path + SPLIT + args_dict.label_file
    total_data = np.load(data_path)
    total_label = np.load(label_path)
    model = paddle.jit.load(os.path.join(model_dir, "model"))
    
    total_correct = 0
    total_acc = 0.0
    total_num = 10000
    for index, data in enumerate(total_data):
        images, labels = total_data[index], total_label[index]
        images = paddle.to_tensor(images)
        if model_name == "efficient-lite4_model/efficient-lite4_model/inference_model":
           images = paddle.reshape(images, [1, 224, 224, 3])
        labels = paddle.to_tensor(labels)
        labels = paddle.reshape(labels, [-1, 1])
        result = model(images)
        result = np.reshape(result, (1000))
        labels = np.reshape(labels, (1))
        correct = post_process(result, top_k=5, labels=labels)
        total_correct += correct

    total_acc = total_correct / total_num
    print("batch_size: {}, predictions top5 accuracy: {}, with total correct {} and total num {}".format(batch_size, total_acc, total_correct, total_num))


def evaluate_infinitensor(base_model_path, onnx_model_name, args_dict):
    from pyinfinitensor.onnx import OnnxStub, backend
    import onnx

    data_path = args_dict.base_path + SPLIT + args_dict.data_file
    label_path = args_dict.base_path + SPLIT + args_dict.label_file
    total_data = np.load(data_path)
    total_label = np.load(label_path)
    model_path = base_model_path + SPLIT + onnx_model_name
    model = onnx.load(model_path)
    # Need to support more devices
    gofusion_model = OnnxStub(model, backend.kunlun_runtime())
    model = gofusion_model
    model.init()

    total_correct = 0
    total_acc = 0.0
    total_num = 10000
    # 使用InfiniTensor的 Runtime 运行刚才加载并转换的模型, 验证是否一致
    for index, data in enumerate(total_data):
        images, labels = total_data[index], total_label[index]
        next(model.inputs.items().__iter__())[1].copyin_float(images.reshape([3*224*224]).tolist())
        model.run()
        outputs = next(model.outputs.items().__iter__())[1].copyout_float()
        outputs = np.reshape(outputs, (1000))
        labels = np.reshape(labels, (1))
        correct = post_process(outputs, top_k=5, labels=labels)
        total_correct += correct
        
        
    total_acc = total_correct / total_num
    print("batch_size: {}, predictions top5 accuracy: {}, with total correct {} and total num {}".format(batch_size, total_acc, total_correct, total_num))

if __name__ == "__main__":
    arg_dict = parse_arg()
    file_name = arg_dict.model_file
    f = open(file_name, "r")
    model_names = []
    for line in f.readlines():
        model_name, onnx_model_name = line.strip().split(" ")
        model_names.append((model_name, onnx_model_name))

    for model_name, onnx_model_name in model_names:
        print("Will test model in this model path:" + arg_dict.base_model_path + "/" + model_name)
        if arg_dict.test_paddle == "True":
            evaluate_paddle(arg_dict.base_model_path, model_name, arg_dict)
        if arg_dict.test_infini_tensor == "True":
            evaluate_infinitensor(arg_dict.base_model_path, onnx_model_name, arg_dict)
        print("-----------------------------------------------------------------------------")