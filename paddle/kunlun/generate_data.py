import os

import argparse
import cv2
import numpy as np
from paddle.io import Dataset
from paddle.vision import transforms as T
import paddle
import time

SPLIT = "/"

transform = T.Compose([
    T.Resize((256, 256)),
    T.CenterCrop(224),
    T.Normalize(mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225],
                data_format="HWC")
])
batch_size = 1

def parse_arg():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument("--base_path",
                        type=str,
                        default=".",
                        nargs="?",
                        help="Base path which we run our project")
    parser.add_argument("--data_path_suffix",
                        type=str,
                        default="datasets/ImageNet/Imagenet_val",
                        nargs="?",
                        help="directory path which we store our data")
    parser.add_argument("--label_path_suffix",
                        type=str,
                        default="datasets/ImageNet/val_list.txt",
                        nargs="?",
                        help="file path which we store our class file")
    

    return parser.parse_args()

class ImageNetDataset(Dataset):
    def __init__(self, data_dir, labels_path, transform=None):
        super().__init__()
        self.labels_dict = generate_classes(labels_path)
        self.data_list = []
        for file_name in os.listdir(data_dir):
            data_path = data_dir + "/" + file_name
            label = self.labels_dict[file_name.split("_")[2].split(".")[0]]
            self.data_list.append([data_path, label])
        self.transform = transform

    def __getitem__(self, index):
        data_path, label = self.data_list[index]
        image = cv2.imread(data_path)
        image = image.astype('float32')
        image = image / 255.

        if self.transform is not None:
            image = transform(image)
        data = np.transpose(image, (2, 0, 1))
        return data, label

    def __len__(self):
        return len(self.data_list)


def generate_classes(class_file_name):
    with open(class_file_name, "r", encoding="utf-8") as f:
        lines = f.readlines()
        labels_dict = {}
        for index, line in enumerate(lines):
            name = line.strip().split()[1]
            label = line.strip().split("_")[2].split(".")[0]
            labels_dict[label] = int(name)
    return labels_dict

def extract_data(args_dict):

    data_path = args_dict.base_path + SPLIT + args_dict.data_path_suffix
    label_path = args_dict.base_path + SPLIT + args_dict.label_path_suffix
    dataset = ImageNetDataset(data_dir=data_path, labels_path=label_path, transform=transform)
    test_loader = paddle.io.DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=0, drop_last=True)

    num = 0
    input_tensors = []
    labels_tensors = []
    for batch_id, data in enumerate(test_loader()):
        images, labels = data
        images = paddle.to_tensor(images)
        labels = paddle.to_tensor(labels)
        labels = paddle.reshape(labels, [-1, 1])
        input_tensors.append(images)
        labels_tensors.append(labels)
        num += 1
        if num == 10000:
            break
    
    np.save('imagenet_images.npy', np.array(input_tensors))
    np.save('imagenet_labels.npy', np.array(labels_tensors))

if __name__ == "__main__":
    arg_dict = parse_arg()
    extract_data(arg_dict)
    print("Extract Data Finished")
