#### How to use these scripts?
##### test_image_classification_model.py
1. Add your paddle model file and onnx model file in model_path.txt file
2. Execute `python test_image_classification_model.py --model_file=model_path.txt`
