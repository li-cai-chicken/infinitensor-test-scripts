# InfiniTensorTestScripts

#### Description
This is a repo to store test scripts for InfiniTensor. InfiniTensor is used to accelerate DL model inference and could get better performance
 than PyTorch and PaddlePaddle on GPU and other national AI accelerators(bang, kunlun, npu).

#### Software Architecture
The architecture of InfiniTensor is simple, the first level folder is classified by compared AI frameworks(PaddlePaddle and PyTorch), 
the second level folder is classified by different AI accelerators. Each file in the folder aims to execute some test cases.

For more details, please refer to the README doc in the folder.

#### Installation

`git clone https://gitee.com/li-cai-chicken/infinitensor-test-scripts.git`

#### Usage Instructions

Please refer to the README doc in the each folder.