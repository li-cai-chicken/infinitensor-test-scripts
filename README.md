# InfiniTensorTestScripts

#### 介绍
这是一个用于存储InfiniTensor测试脚本的仓库。InfiniTensor可用于加速深度学习模型推理，并且在GPU和其他国产AI加速器寒武纪、昆仑芯和昇腾(暂未支持)上可以获得比PyTorch和PaddlePaddle更好的性能。

#### 软件架构
InfiniTensorTestScripts的架构很简单，一级文件夹按照不同的对比AI框架（PaddlePaddle 和 PyTorch）分类，二级文件夹按照不同的AI加速器分类。每个文件夹中的文件旨在执行一些测试用例。

更多介绍，请参考子文件夹中的README文档。

#### 安装教程

`git clone https://gitee.com/li-cai-chicken/infinitensor-test-scripts.git`

#### 使用说明

请参考每个文件夹中的README文档。