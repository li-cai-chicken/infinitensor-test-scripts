import os
import onnx
import numpy as np
import time
import onnxruntime
import logging
from pyinfinitensor.onnx import OnnxStub
from pyinfinitensor import backend

# Set up logging with .log extension
log_file_path = '/workspace/InfiniTensor/test/onnx/inference_onnxfile2.log'
logging.basicConfig(filename=log_file_path, level=logging.INFO)

folder_path = '/workspace/onnxfile2'
files_and_folders = os.listdir(folder_path)

for item in files_and_folders:
    item_path = os.path.join(folder_path, item)

    try:
        logging.info("-------------------------------------------------------------------------------")
        logging.info("Test {}".format(item))

        # Inference with Infinitensor
        model_infinitensor = onnx.load(item_path)
        gofusion_model = OnnxStub(model_infinitensor, backend.bang_runtime())
        model_infinitensor = gofusion_model
        model_infinitensor.init()
        x_infinitensor = np.random.random((1, 3, 224, 224)).astype('float32')
        next(model_infinitensor.inputs.items().__iter__())[1].copyin_float(x_infinitensor.reshape([3*224*224]).tolist())
        start_time_infinitensor = time.time()
        model_infinitensor.run()
        end_time_infinitensor = time.time()
        outputs_infinitensor = next(model_infinitensor.outputs.items().__iter__())[1].copyout_float()
        logging.info('Exported {} Predict finished by Infinitensor!'.format(item))

        # Predict by ONNXRuntime
        onnxruntime_session = onnxruntime.InferenceSession(item_path)
        if item == "onnxfile/efficientnet-lite4-11.onnx":
            ort_inputs = {onnxruntime_session.get_inputs()[0].name: x_infinitensor.reshape([1, 224, 224, 3])}
        else:
            ort_inputs = {onnxruntime_session.get_inputs()[0].name: x_infinitensor}
        ort_outputs = onnxruntime_session.run(None, ort_inputs)
        logging.info("Exported {} has been predicted by ONNXRuntime!".format(item))
        
        # Compare ONNXRuntime and Infinitensor results
        try:
            np.testing.assert_allclose(ort_outputs[0].reshape([1000]), outputs_infinitensor, rtol=1.0, atol=1e-02)
            logging.info("The difference of results between ONNXRuntime and Infinitensor looks good!")   
        except AssertionError as e:
            logging.error("Onnx and Infinitensor compare gap: {}".format(str(e)))

    except Exception as e:
        logging.error("AI framework has some issues during inference for model {}.".format(item))
        logging.error("Error details: {}".format(str(e)))

