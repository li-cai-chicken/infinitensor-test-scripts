import sys
import onnx
import time
import numpy as np
import torch
from pyinfinitensor.onnx import OnnxStub, backend

if __name__ == "__main__":
    args = sys.argv
    if len(sys.argv) != 2:
        print("Usage: python dual_inference.py model_name.onnx")
        exit()

    model_path = sys.argv[1]
    onnx_model = onnx.load(model_path)
    onnx_input = onnx_model.graph.input[0]
    
    input_shape = [[d.dim_value for d in _input.type.tensor_type.shape.dim] for _input in onnx_model.graph.input]
    input_shape = input_shape[0]  # Assume that there is only one input tensor
    
    datatype = np.float32
    if "gpt" in model_path:
        datatype = np.int64

    input_data = np.random.random(input_shape).astype(datatype)

    # PyTorch Model
    pytorch_model = torch.nn.Sequential(
        torch.nn.Linear(input_shape[-1], 512),
        torch.nn.ReLU(),
        torch.nn.Linear(512, 256),
        torch.nn.ReLU(),
        torch.nn.Linear(256, 10)  # Adjust output size to match input size
    )
    
    input_data_torch = torch.from_numpy(input_data).to(torch.float32)
    pytorch_model.to(torch.float32)

    # InfiniTensor Model
    infinitensor_model = OnnxStub(onnx_model, backend.kunlun_runtime())

    total_time_pytorch = 0.0
    total_time_infinitensor = 0.0

    # PyTorch Inference
    print("PyTorch Inference Times:")
    for index in range(100):
        start_time_pytorch = time.time()
        with torch.no_grad():
            pytorch_outputs = pytorch_model(input_data_torch)
        end_time_pytorch = time.time()
        total_time_pytorch += (end_time_pytorch - start_time_pytorch)
        print(f'Inference {index + 1}: {end_time_pytorch - start_time_pytorch} s')

    # Print average inference time with PyTorch
    print('\nAverage PyTorch inference time: %f s' % (total_time_pytorch / 100))

    # InfiniTensor Inference
    # Copy input data to InfiniTensor
    next(iter(infinitensor_model.inputs.values())).copyin_numpy(input_data)

    total_time_infinitensor = 0.0

    # InfiniTensor Inference
    print("\nInfiniTensor Inference Times:")
    for index in range(100):
        start_time_infinitensor = time.time()
        infinitensor_model.run()
        end_time_infinitensor = time.time()
        total_time_infinitensor += (end_time_infinitensor - start_time_infinitensor)
        print(f'Inference {index + 1}: {end_time_infinitensor - start_time_infinitensor} s')

    # Print average inference time with InfiniTensor
    print('\nAverage InfiniTensor inference time: %f s' % (total_time_infinitensor / 100))
